package com.example.retrofitdemo

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface APIService {
    @GET("/api/1/news")
    fun getNews(
        @Query("apikey") apiKey: String,
        @Query("country") country: String
    ): Call<NewsApiResponse>
}
