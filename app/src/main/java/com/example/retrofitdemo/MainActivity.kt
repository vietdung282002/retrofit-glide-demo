package com.example.retrofitdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

const val TAG = "Check json"
class MainActivity : AppCompatActivity(), Callback<NewsApiResponse> {
    private var mNews : MutableList<News> = mutableListOf()
    private lateinit var recyclerView: RecyclerView

    private val apiKey = "pub_346686d858d935e0f2a5dc312b11624d63171"
    private val country = "vi"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView = findViewById(R.id.rvNews)
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.adapter = NewsAdapter(applicationContext,mNews)
        initData()
    }

    private fun initData() {
        API.apiService.getNews(apiKey,country).enqueue(this)
    }

    override fun onResponse(call: Call<NewsApiResponse>, response: Response<NewsApiResponse>) {
        if( response.body()==null){
            return
        }
        for(news in response.body()!!.results ){
            mNews.add(news)
            if(news.image_url == null){
                Log.d(TAG, "onResponse: ")
            }
        }
        
        recyclerView.adapter!!.notifyDataSetChanged()
    }

    override fun onFailure(call: Call<NewsApiResponse>, t: Throwable) {
        Toast.makeText(applicationContext,"Error",Toast.LENGTH_SHORT).show()
    }

}